import React from 'react';
import { Row, Col } from 'antd';
import AboutTile from '../../AbouTile';
import { stripTags, domHtml } from '../../../utils/stripTags';
import SEO from '../../Seo';

const pageText = {
  paraOne: `Bonjour, je m'appelle Charles-Edouard Toutain je suis développeur Full stack et chef de projet Odoo.
  Je développe sur Odoo en python et JavaScript, mais aussi j'aime les nouvelles technologies notamment les CMS headless et construire des applications rapides et performantes
  Je suis aussi très sensible au besoin fonctionnelle et métier.`,
  paraTwo: `Actuellement je travail chez Subteno IT, intégrateur Odoo gold partner. Je réalise des missions de développement Web et logiciel sur le framework Odoo, aussi j'ai en charge
  la gestion de projet E-commerce. Je termine ma deuxième année de master "management de projets numériques" en alternance. Aussi je suis à l'écoute de différente opportunité en free-lancing car j'ai 
  aussi une activité en micro-entreprise ou je créé des sites Web/E-Commerce avec WordPress.`,
};
const AboutMe = () => {
  const description = `${pageText.paraOne} ${stripTags(pageText.paraTwo)}`;
  return (
    <>
      <div>
        <SEO
          title="About"
          description={description}
          path=""
          keywords={['Toutain', 'Charles-Edouard', 'Alençon', 'FullStack developer', 'Javascript', 'ReactJS', 'NodeJS', 'Gatsby', 'Python', 'Odoo']}
        />
        <h1 className="titleSeparate">A propos</h1>
        <p>
          {pageText.paraOne}
        </p>
        <p dangerouslySetInnerHTML={domHtml(pageText.paraTwo)} />
      </div>
      <Row gutter={[20, 20]}>
        <Col xs={24} sm={24} md={12} lg={8}>
          <AboutTile
            img="location.png"
            height={60}
            alt="location image"
            textH4="Mobilité"
            textH3="France et international"
          />
        </Col>
        <Col xs={24} sm={24} md={12} lg={8}>
          <AboutTile
            img="web.png"
            alt="web image"
            textH4="Technophile"
            textH3="J'aime faire de la veille technique"
          />
        </Col>
        <Col xs={24} sm={24} md={12} lg={8}>
          <AboutTile
            img="meeting.png"
            alt="meeting image"
            textH4="Relation client"
            textH3="Primordial pour moi"
          />
        </Col>
        <Col xs={24} sm={24} md={12} lg={8}>
          <AboutTile
            img="location.png"
            alt="motorcycle image"
            textH4="Remote"
            textH3="Je peux télétravailler ou travailler sur site"
          />
        </Col>
        <Col xs={24} sm={24} md={12} lg={8}>
          <AboutTile
            img="web.png"
            alt="web image"
            textH4="Auto didacte"
            textH3="Je suis régulièrement des cours openclassrooms"
            height={60}
            width={60}
          />
        </Col>
        <Col xs={24} sm={24} md={12} lg={8}>
          <AboutTile
            img="graduation.png"
            alt="graduation image"
            textH4="Diplômé"
            textH3="Master en management de projet numériques"
            height={60}
            width={60}
          />
        </Col>
      </Row>
    </>
  );
};
export default AboutMe;
